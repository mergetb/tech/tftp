#!/bin/bash

set -e

docker build -f debian/builder.dock -t tftp-builder .
docker run -v `pwd`:/tftp tftp-builder /tftp/build-deb.sh
