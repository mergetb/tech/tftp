#!/bin/bash

TARGET=${TARGET:-amd64}
DEBUILD_ARGS=${DEBUILD_ARGS:-""}

rm -f build/merge-tftp*.build*
rm -f build/merge-tftp*.change
rm -f build/merge-tftp*.deb

debuild -e V=1 -e prefix=/usr $DEBUILD_ARGS -i -us -uc -b

mv ../merge-tftp*.build* build/
mv ../merge-tftp*.changes build/
mv ../merge-tftp*.deb build/
