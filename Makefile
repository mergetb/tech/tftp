prefix ?= /usr/local

VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X main.Version=$(VERSION)"

.PHONY: all
all: build/merge-tftp

build/merge-tftp: *.go | build
	$(QUIET) $(call go-build)

REGISTRY ?= docker.io
REPO ?= mergetb
COMMIT ?= $(shell git log -1 --format=%h)
TAG ?= $(shell git tag --points-at $(COMMIT))

.PHONY: $(REGISTRY)/$(REPO)/tftp | build
$(REGISTRY)/$(REPO)/tftp: container/Dockerfile build/merge-tftp
	$(docker-build)

.PHONY: container
container: $(REGISTRY)/$(REPO)/tftp | build

.PHONY: clean
clean:
	$(QUIET) rm -rf build

build:
	mkdir -p build

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

QUIET=@
DOCKER_QUIET=-q
ifeq ($(V),1)
	QUIET=
	DOCKER_QUIET=
endif

.PHONY: install
install:
	$(QUIET) install -D build/merge-tftp $(DESTDIR)$(prefix)/bin/merge-tftp


define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) \
		go build -ldflags=${LDFLAGS} -o $@ $(dir $<)*.go
endef

define docker-build
	$(call build-slug,docker)
	$(QUIET) docker build ${BUILD_ARGS} $(DOCKER_QUIET) -f $< -t $(@):$(COMMIT) .
	$(if ${PUSH},$(call docker-push))
endef

# only push one container, either the commit if tag is not set
# or the tag when it is set
define docker-push
	$(call build-slug,push)
	$(if ${TAG},,$(QUIET) docker push $(@):$(COMMIT))
	$(if ${TAG},docker tag $(@):$(COMMIT) $(@):$(TAG))
	$(if ${TAG},$(QUIET) docker push $(@):$(TAG))
endef
