package main

import (
	"io"
	"log"
	"net"
	"os"

	"go.universe.tf/netboot/tftp"
)

var version = "undefined"

func main() {

	log.Printf("mergetb-tftp %s", version)

	server := new(tftp.Server)
	server.Handler = handler
	log.Fatal(server.ListenAndServe("0.0.0.0:69"))

}

func handler(path string, clientAddr net.Addr) (
	file io.ReadCloser, size int64, err error) {

	log.Printf("request: %s -> %s", clientAddr, path)

	f, err := os.Open("/srv/" + path)
	if err != nil {
		log.Printf("error open: %v", err)
		return nil, 0, err
	}

	info, err := f.Stat()
	if err != nil {
		log.Printf("error stat: %v", err)
		return nil, 0, err
	}

	return f, info.Size(), err

}
